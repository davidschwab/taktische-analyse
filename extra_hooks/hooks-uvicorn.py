"""
Notwendig um die Anwendung als executable auszuliefern
"""

from PyInstaller.utils.hooks import collect_submodules

hiddenimports = collect_submodules('uvicorn')
