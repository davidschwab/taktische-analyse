import numpy as np

FIELD_SHAPE = (4, 5)


def interpolate_from_ones(db_weight_matrix: np.ndarray):  # Change: lists can't be indexed with tuples
    weight_matrix = db_weight_matrix.copy()
    ones_xy = []
    for y in range(len(weight_matrix)):
        for x in range(len(weight_matrix[0])):
            if weight_matrix[y, x] == 1:
                ones_xy.append([x, y])

    for y in range(len(weight_matrix)):
        for x in range(len(weight_matrix[0])):
            if [x, y] in ones_xy:
                continue
            interpolated_weight = 0
            for [i, j] in ones_xy:
                interpolated_weight += 0.25 ** np.sqrt((x - i) ** 2 + (y - j) ** 2)
            weight_matrix[y, x] = min(1, interpolated_weight)

    return weight_matrix


def get_position_names_by_position_ids(position_ids: list[int]):
    position_names = []
    for position_id in position_ids:
        match position_id:
            case 1:
                position_names.append('left_outside_back')
            case 2:
                position_names.append('right_outside_back')
            case 3 | 4 | 5 | 6 | 7:
                position_names.append('center_back')
            case 8 | 9 | 10:
                position_names.append('defensive_midfielder')
            case 11:
                position_names.append('left_midfielder')
            case 12:
                position_names.append('right_midfielder')
            case 13 | 14 | 15 | 16:
                position_names.append('midfielder')
            case 17 | 18 | 19:
                position_names.append('attacking_midfielder')
            case 20:
                position_names.append('left_winger')
            case 21:
                position_names.append('right_winger')
            case 22 | 23 | 24:
                position_names.append('center_forward')

    return position_names
