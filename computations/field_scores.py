import numpy as np
from ortools.linear_solver import pywraplp
from ortools.sat.python import cp_model

FIELD_SHAPE = (4, 5)


def compute_player_field_score(ratings: list[dict], attribute_weight_matrices) -> np.ndarray:
    if len(ratings) == 0:
        return np.zeros(FIELD_SHAPE)
    field_score = np.zeros(FIELD_SHAPE)
    attribute_weight_matrices = {item['name']: item for item in attribute_weight_matrices}
    number_of_attributes = 0
    overall_mean = 0
    nan_attributes = []
    for key in ratings[0].keys():
        # skip attributes that do not contribute to the field score
        if key not in attribute_weight_matrices:
            continue
        # compute means for each attribute
        mean = np.mean([r[key] for r in ratings if r[key] is not None])
        if not np.isnan(mean):
            # add the contribution of each attribute to the field score matrix
            field_score += mean * np.array(attribute_weight_matrices[key]['array'])
            number_of_attributes += 1
            overall_mean += mean
        else:
            nan_attributes.append(key)
    
    overall_mean /= number_of_attributes
    for key in nan_attributes:
        field_score += overall_mean * np.array(attribute_weight_matrices[key]['array'])
    
    field_score /= np.sum(np.stack([np.array(attribute_weight_matrices[key]['array'])
                            for key in ratings[0].keys() if key in attribute_weight_matrices],
                            axis=0), axis=0)
    return field_score


def compute_player_position_scores(ratings: list[dict], attribute_weight_matrices, position_weight_matrices):
    field_score = compute_player_field_score(ratings, attribute_weight_matrices)
    position_scores = []
    for position_matrix in position_weight_matrices:
        position_scores.append((field_score * position_matrix['array']).tolist())

    return position_scores


def compute_best_team_field_score(list_of_player_ratings: list[list[dict]],
                                  attribute_weight_matrices, position_weight_matrices):
    assert len(position_weight_matrices) == 10  # ten field players/positions to assign
    team_score = np.zeros(FIELD_SHAPE)
    player_scores = np.zeros((len(list_of_player_ratings), len(position_weight_matrices)))

    for i in range(len(list_of_player_ratings)):
        field_score = compute_player_field_score(list_of_player_ratings[i], attribute_weight_matrices)
        for j in range(len(position_weight_matrices)):
            position_score = field_score * position_weight_matrices[j]
            tmp = np.mean(position_score[position_score != 0])
            player_scores[i, j] = 0 if np.isnan(tmp) else tmp

    # For comparison, the worst score
    compute_best_assignment(player_scores, best=False)
    # Best score
    positions_to_players = compute_best_assignment(player_scores)
    assert positions_to_players is not None
    chosen_players = []

    for i in range(len(positions_to_players)):
        position, player = positions_to_players[i]
        chosen_players.append(player)
        field_score = compute_player_field_score(list_of_player_ratings[player], attribute_weight_matrices)
        position_score = field_score * position_weight_matrices[position]
        team_score += position_score

    # normalization todo
    # easily interpretable without normalization

    return chosen_players, team_score


# Also works with field scores, if supplied in correct format
def compute_best_assignment(player_scores, player_names=None, position_names=None, quantization=1e5,
                            best=True, print_solution=True):
    def printd(*args):
        return print(*args) if print_solution else None

    assignment = cp_model.CpModel()
    player_scores_arr = np.array(player_scores)
    assert len(player_scores_arr.shape) == 2
    num_players, num_positions = player_scores_arr.shape

    x = []
    for i in range(num_players):
        t = []
        for j in range(num_positions):
            t.append(assignment.NewBoolVar(f'x[{i},{j}]'))
        x.append(t)

    # Each player is assigned to at most 1 position.
    for i in range(num_players):
        assignment.AddAtMostOne(x[i][j] for j in range(num_positions))

    # Each position is assigned to exactly one player.
    for j in range(num_positions):
        assignment.AddExactlyOne(x[i][j] for i in range(num_players))

    objective_terms = []
    for player in range(num_players):
        for position in range(num_positions):
            objective_terms.append(int(1 + player_scores_arr[player][position] * quantization) * x[player][position])

    assignment.Maximize(sum(objective_terms)) if best else assignment.Minimize(sum(objective_terms))
    solver = cp_model.CpSolver()
    solution_printer = VarArraySolutionPrinter(sum(x, []))  # Unused currently, for debug
    status = solver.Solve(assignment)

    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        positions_to_players = []
        for j in range(num_positions):
            for i in range(num_players):
                if solver.BooleanValue(x[i][j]):
                    printd(f'{"Player index " + str(i) if player_names is None else player_names[i]} assigned to '
                           f'position index {j if position_names is None else position_names[j]}. '
                           f'Score = {player_scores_arr[i][j]}')
                    positions_to_players.append((j, i))
        if best:
            printd(f'Total score = {solver.ObjectiveValue() / quantization / num_positions}\n')
        else:
            printd(f'Total score (worst possible!) = {solver.ObjectiveValue() / quantization / num_positions}\n')
        return positions_to_players
    else:
        printd('No solution found.')
        return None


class VarArraySolutionPrinter(cp_model.CpSolverSolutionCallback):
    def __init__(self, variables):
        cp_model.CpSolverSolutionCallback.__init__(self)
        self.__variables = variables
        self.__solution_count = 0

    def on_solution_callback(self):
        self.__solution_count += 1
        for v in self.__variables:
            if self.Value(v) != 0:
                print('%s=%i' % (v, self.Value(v)), end=' ')
        print()

    def solution_count(self):
        return self.__solution_count


def compute_best_assignment_SCIP(player_scores: list[list[int]], player_names=None, position_names=None,
                                 quantization=1e5):
    assignment = pywraplp.Solver.CreateSolver('SCIP')
    player_scores_arr = np.array(player_scores)
    assert len(player_scores_arr.shape) == 2
    num_players, num_positions = player_scores_arr.shape

    x = {}
    for i in range(num_players):
        for j in range(num_positions):
            x[i, j] = assignment.IntVar(0, 1, '')

    # Each player is assigned to at most 1 position.
    for i in range(num_players):
        assignment.Add(assignment.Sum([x[i, j] for j in range(num_positions)]) <= 1)

    # Each position is assigned to exactly one player.
    for j in range(num_positions):
        assignment.Add(assignment.Sum([x[i, j] for i in range(num_players)]) == 1)

    objective_terms = []
    for player in range(num_players):
        for position in range(num_positions):
            objective_terms.append(int(player_scores_arr[player][position] * quantization) * x[player, position])

    assignment.Maximize(assignment.Sum(objective_terms))
    status = assignment.Solve()

    if status == pywraplp.Solver.OPTIMAL:
        print(f'Total score = {assignment.Objective().Value() / quantization / num_positions}\n')
        for i in range(num_players):
            for j in range(num_positions):
                if x[i, j].solution_value() > 0.5:
                    print(f'{"Player " + str(i) if player_names is None else player_names[i]} assigned to '
                          f'position {j if position_names is None else position_names[j]}. '
                          f'Score = {player_scores_arr[i][j]}')
    else:
        print('No solution found.')
