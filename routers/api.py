"""
Hier definieren wir die RESTapi, mit der das Frontend interagieren kann
"""
from fastapi import APIRouter, Response, status

from computations.weight_matrices import get_position_names_by_position_ids
from computations.field_scores import compute_player_field_score, compute_player_position_scores, compute_best_team_field_score
from model.DBInterface import DBInterface
from model.DTO import *

NUM_ATTRIBUTES = 35

api = APIRouter(prefix="/api")

# might need to switch to aio interface, lets see for now
dbi = DBInterface("sqlite+pysqlite:///db.sqlite")


# Players

@api.get("/players")
async def get_players():
    return dbi.get_players()


@api.post("/player")
async def add_player(player: PlayerDTO):
    return dbi.create_player(player)


@api.get("/player/{player_id}")
async def get_player(player_id: int):
    return dbi.get_player_by_id(player_id)


@api.put("/player/{player_id}")
async def add_player(player_id: int, player: PlayerDTO):
    return dbi.update_player(player_id, player)


@api.delete("/player")
async def delete_player(player_id: int):
    dbi.delete_player(player_id)
    return player_id


# Ratings

@api.post("/ratings")
async def add_rating(rating: RatingDTO):
    return dbi.create_rating(rating)


@api.get("/ratings")
async def get_ratings():
    return dbi.get_ratings()


@api.get("/ratings/{player_id}")
async def get_ratings_by_player_id(player_id: int):
    return dbi.get_ratings_by_player_id(player_id)

@api.delete("/ratings/{rating_id}")
async def delete_rating_by_rating_id(rating_id: int):
    return dbi.delete_rating_by_id(rating_id)


# Field Scores

@api.get("/field_score/{player_id}")
async def get_field_score(player_id: int, attribute_preset: str = "Default_Ratings"):
    ratings = dbi.get_ratings_by_player_id(player_id)

    # if player has no ratings return empty field score
    if not ratings:
        return [[0, 0, 0, 0, 0]] * 4

    attribute_weight_matrices = dbi.get_preset_dict_by_name(attribute_preset, interpolate=True)['matrices']
    attribute_weight_matrices = [matrix.copy() for matrix in attribute_weight_matrices]

    field_score = compute_player_field_score(ratings, attribute_weight_matrices)

    # convert to list, since numpy arrays are not serializable
    return field_score.tolist()


@api.get("/position_scores/{player_id}")
async def get_position_scores(player_id: int, attribute_preset: str, position_preset: str):
    ratings = dbi.get_ratings_by_player_id(player_id)

    position_weight_matrices = dbi.get_preset_dict_by_name(position_preset, interpolate=True)['matrices']
    position_weight_matrices = [matrix.copy() for matrix in position_weight_matrices]

    # if player has no ratings return empty field score
    if not ratings:
        position_scores = [[[0, 0, 0, 0, 0]] * 4] * 12
    else:
        attribute_weight_matrices = dbi.get_preset_dict_by_name(attribute_preset, interpolate=True)['matrices']
        attribute_weight_matrices = [matrix.copy() for matrix in attribute_weight_matrices]

        position_scores = compute_player_position_scores(ratings, attribute_weight_matrices, position_weight_matrices)

    return {position_matrix['name']: position_scores[index] for index, position_matrix in enumerate(position_weight_matrices)}


@api.post("/compute_formation")
async def get_best_team_formation(formation_configuration: FormationConfigurationDTO):
    player_ids = formation_configuration['player_ids']
    position_ids = formation_configuration['position_ids']

    ratings: dict = {player_id: dbi.get_ratings_by_player_id(player_id) for player_id in player_ids}

    attribute_weight_matrices = dbi.get_preset_dict_by_name(
        formation_configuration['attribute_preset'], interpolate=True
    )['matrices']

    attribute_weight_matrices = [matrix.copy() for matrix in attribute_weight_matrices]

    position_names = get_position_names_by_position_ids(position_ids)

    position_weight_matrices = dbi.get_preset_dict_by_name(
        formation_configuration['position_preset'], interpolate=True
    )['matrices']

    position_weight_matrices = [
        [
            position_weight_matrices[x]['array'].copy() for x in range(len(position_weight_matrices))
            if position_weight_matrices[x]['name'] == position_name
        ]
        for position_name in position_names
    ]

    for i, position_weight_matrix in enumerate(position_weight_matrices):
        assert len(position_weight_matrix) == 1
        position_weight_matrices[i] = position_weight_matrix[0]

    player_indices, field_score = compute_best_team_field_score(list(ratings.values()),
                                                                attribute_weight_matrices,
                                                                position_weight_matrices)

    chosen_players = [{
        'player_id': player_ids[index], 'position_id': position_ids[enumerator]
    } for enumerator, index in enumerate(player_indices)]

    return {'chosen_players': chosen_players, 'field_score': field_score.tolist()}


# Weight Matrices

@api.put("/weight_matrix")
async def add_weight_matrix(weight_matrix_dto: WeightMatrixDTO):
    return {"matrix_id": dbi.add_weight_matrix(weight_matrix_dto)}


@api.put("/weight_matrix/{matrix_id}")
async def edit_weight_matrix(matrix_id: int, weight_matrix_dto: WeightMatrixDTO, response: Response):
    matrix = get_weight_matrix(matrix_id, response)
    if not matrix:
        return
    matrix = dbi.update_weight_matrix(matrix_id, weight_matrix_dto)
    return "Matrix updated", matrix


@api.get("/weight_matrix/{matrix_id}")
async def get_weight_matrix(matrix_id: int, response: Response, interpolate: bool = False):
    matrix = dbi.get_weight_matrix_by_id(matrix_id)
    if matrix:
        return matrix.to_dict(interpolate)
    else:
        response.status_code = status.HTTP_404_NOT_FOUND


@api.delete("/weight_matrix/{matrix_id}")
async def del_weight_matrix(matrix_id: int, response: Response):
    matrix = get_weight_matrix(matrix_id, response)
    if not matrix:
        return
    deleted_id = dbi.delete_weight_matrix(matrix_id)
    return f"Deleted matrix {deleted_id}"


# Presets


@api.get("/presets")
async def get_presets():
    return dbi.get_all_preset_dicts()


@api.get("/rating_presets")
async def get_presets():
    return [entry for entry in dbi.get_all_preset_dicts() if not entry["is_room_preset"]]


@api.get("/room_presets")
async def get_room_presets():
    return [entry for entry in dbi.get_all_preset_dicts() if entry["is_room_preset"]]


@api.get("/preset/{name}")
async def get_preset(name: str, response: Response, interpolate_matrices: bool = False):
    preset = dbi.get_preset_dict_by_name(name, interpolate_matrices)
    if preset:
        return preset
    else:
        response.status_code = status.HTTP_404_NOT_FOUND


@api.put("/preset/{name}")
async def add_preset(name: str, is_room_preset: bool, weight_matrix_ids: list[int], response: Response):
    preset = dbi.get_preset_dict_by_name(name)
    if preset:
        response.status_code = status.HTTP_405_METHOD_NOT_ALLOWED
        return "Name already in use."
    return dbi.add_preset(name, is_room_preset, weight_matrix_ids)


@api.delete("/preset/{name}")
async def delete_preset(name: str, response: Response):
    preset = dbi.get_preset_dict_by_name(name)
    if preset:
        deleted_name = dbi.delete_preset(name)
        return f"Deleted preset {deleted_name}"
    response.status_code = status.HTTP_404_NOT_FOUND


@api.post("/preset_with_matrices")
async def create_preset_with_matrices(preset: PresetDTO, response: Response):
    matrices = preset['matrices']
    if dbi.get_preset_dict_by_name(preset['name']):
        response.status_code = status.HTTP_405_METHOD_NOT_ALLOWED
        return "Name already in use."
    matrix_ids = [await add_weight_matrix(matrix) for matrix in matrices]
    matrix_ids = [matrix['matrix_id'] for matrix in matrix_ids]

    return dbi.add_preset(preset['name'], preset["is_room_preset"], matrix_ids)
