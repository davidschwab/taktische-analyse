/**
 * api requests related to presets
 */
import {api} from 'boot/axios';
import {Preset} from 'stores/interfaces';

export default {
  get(name: string, interpolate: boolean) {
    return api.get('preset/' + name, {params: {interpolate_matrices: interpolate}});
  },
  getAll(type: string) {
    if (type == 'attribute') {
      return api.get('rating_presets');
    } else if (type == 'position') {
      return api.get('room_presets');
    } else {
      return api.get('presets');
    }
  },
  add(preset: Preset) {
    return api.post('preset_with_matrices', preset);
  },
  edit(preset: Preset) {
    preset.matrices.forEach(matrix => {
      api.put('/weight_matrix/' + matrix.id, matrix).then()
    })
    return api.get('preset/' + preset.name);
  },
  delete(name: string) {
    return api.delete('preset/' + name);
  }
}
