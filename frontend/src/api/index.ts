import players from './players'
import ratings from './ratings'
import presets from './presets'

export default {
  players,
  ratings,
  presets
}
