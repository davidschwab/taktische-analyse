/**
 * api requests related to ratings
 */
import {api} from 'boot/axios';
import {Rating} from 'stores/interfaces';

export default {
  get(id: number) {
    return api.get('ratings/' + id);
  },
  getAll() {
    return api.get('ratings');
  },
  addRating(rating: Rating) {
    delete rating.creation_datetime
    return api.post('ratings', rating);
  },
  deleteRating(rating_id: number) {
    return api.delete('ratings/' + rating_id)
  }
}
