/**
 * api requests related to players
 */
import {api} from 'boot/axios';
import {Player} from 'stores/interfaces';

export default {
  get(id: number) {
    return api.get('player/' + id);
  },
  getAll() {
    return api.get('players');
  },
  addPlayer(player: Player) {
    delete player.id
    return api.post('player', player);
  },
  edit(player: Player) {
    const id = player.id
    delete player.id
    return api.put('player/' + id, player);
  },
  delete(id: number) {
    return api.delete('player', {params: {player_id: id}});
  },
  getFieldScore(id: number, preset: string) {
    return api.get('field_score/' + id, {params: {attribute_preset: preset}})
  },
  getPositionScores(id: number, attributePreset: string, positionPreset: string) {
    return api.get('position_scores/' + id, {
      params: {
        attribute_preset: attributePreset,
        position_preset: positionPreset
      }
    })
  },
  getBestFormation(playerIds: number[], attributePreset: string, positionPreset: string, positionIds: number[]) {
    const formation_configuration = {
      'player_ids': playerIds,
      'position_ids': positionIds,
      'attribute_preset': attributePreset,
      'position_preset': positionPreset
    }
    return api.post('compute_formation', formation_configuration)
  }
}
