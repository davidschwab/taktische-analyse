import {RouteRecordRaw} from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '', component: () => import('pages/IndexPage.vue')},
      {path: 'player', component: () => import('pages/players/PlayersOverview.vue')},
      {path: 'player/:id', component: () => import('pages/players/PlayerPage.vue')},
      {path: 'start11', component: () => import('pages/Start11Page.vue')},
      {path: 'matrices', component: () => import('pages/settings/MatricesPage.vue')},
      {path: 'matrices/attributePresets', component: () => import('pages/settings/AttributePresets.vue')},
      {path: 'matrices/positionPresets', component: () => import('pages/settings/PositionPresets.vue')},
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
