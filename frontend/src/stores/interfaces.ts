export interface Player {
  id?: number | null,
  first_name: string,
  last_name: string,
  sex: string,
  weight: number | null,
  height: number | null,
  date_of_birth: string,
  tricot_number: number | null,
  past_positions: string,

  // age: number,
  // right_foot: number,
  // left_foot: number,
}

export interface WeightMatrix {
  id?: number,
  name: string,
  array: number[][]
}

export interface Preset {
  name: string,
  is_room_preset?: boolean
  matrices: WeightMatrix[]
}

export const attributes = ['acceleration', 'agility', 'balance', 'endurance', 'jumping_reach', 'pace', 'strength', 'corners', 'dribbling', 'finishing', 'first_touch', 'heading', 'marking', 'ranged_passing', 'shooting_accuracy', 'shooting_power', 'short_passing', 'tackling', 'technique', 'aggression', 'anticipation', 'bravery', 'composure', 'concentration', 'consistency', 'creativity', 'decisions', 'determination', 'flair', 'mental_resistance', 'leadership', 'positioning', 'teamwork', 'vision', 'work_rate']
export const positions = ['goalkeeper', 'left_outside_back', 'right_outside_back', 'center_back', 'defensive_midfielder', 'left_midfielder', 'right_midfielder', 'midfielder', 'attacking_midfielder', 'left_winger', 'right_winger', 'center_forward']

export interface Rating extends Record<string, any> {
  'id'?: number,
  'player_id': number,
  'creation_datetime'?: number | null,
  'time_active': number | null,
  'aggression': number | null,
  'anticipation': number | null,
  'bravery': number | null,
  'composure': number | null,
  'concentration': number | null,
  'consistency': number | null,
  'decisions': number | null,
  'determination': number | null,
  'creativity': number | null,
  'flair': number | null,
  'mental_resistance': number | null,
  'leadership': number | null,
  'positioning': number | null,
  'teamwork': number | null,
  'vision': number | null,
  'work_rate': number | null,
  'acceleration': number | null,
  'agility': number | null,
  'balance': number | null,
  'jumping_reach': number | null,
  'endurance': number | null,
  'pace': number | null,
  'strength': number | null,
  'corners': number | null,
  'dribbling': number | null,
  'finishing': number | null,
  'first_touch': number | null,
  'heading': number | null,
  'shooting_accuracy': number | null,
  'shooting_power': number | null,
  'marking': number | null,
  'short_passing': number | null,
  'ranged_passing': number | null,
  'tackling': number | null,
  'technique': number | null,
  'free_kicks': number | null,
  'penalties': number | null,
  'long_throws': number | null
}

export const rating_classes = {
  // Set_Piece: ['free_kicks', 'penalties', 'long_throws'],
  Physical: ['acceleration', 'agility', 'balance', 'endurance', 'jumping_reach', 'pace', 'strength'],
  Technical: ['corners', 'dribbling', 'finishing', 'first_touch', 'heading', 'marking', 'ranged_passing', 'shooting_accuracy', 'shooting_power', 'short_passing', 'tackling', 'technique'],
  Mental: ['aggression', 'anticipation', 'bravery', 'composure', 'concentration', 'consistency', 'creativity', 'decisions', 'determination', 'flair', 'mental_resistance', 'leadership', 'positioning', 'teamwork', 'vision', 'work_rate']
}

export interface Position {
  id: number,
  title: string,
  x: number,
  y: number
}

export const Positions: Position[] = [
  {id: 0, title: 'Goalkeeper', x: 0.5, y: 0.91},
  // defense positions -------------------------------------------------------------------------------------------------
  // 4-chain defense
  {id: 1, title: 'Left Outside Back', x: 0.11, y: 0.754},
  {id: 2, title: 'Right Outside Back', x: 0.89, y: 0.754},
  {id: 3, title: 'Left Center Back', x: 0.37, y: 0.789},
  {id: 4, title: 'Right Center Back', x: 0.63, y: 0.789},
  // 3-chain defense
  {id: 5, title: 'Center Back', x: 0.5, y: 0.79},
  {id: 6, title: 'Left Back', x: 0.235, y: 0.78},
  {id: 7, title: 'Right Back', x: 0.765, y: 0.78},
  // middle positions --------------------------------------------------------------------------------------------------
  // backwards
  {id: 8, title: 'Defensive Left Midfielder', x: 0.37, y: 0.5784},
  {id: 9, title: 'Defensive Right Midfielder', x: 0.63, y: 0.5784},
  {id: 10, title: 'Defensive Center Midfielder', x: 0.5, y: 0.5784},
  // center-line middle
  {id: 11, title: 'Left Outside Midfielder', x: 0.11, y: 0.5},
  {id: 12, title: 'Right Outside Midfielder', x: 0.89, y: 0.5},
  {id: 13, title: 'Left Midfielder', x: 0.294, y: 0.5},
  {id: 14, title: 'Right Midfielder', x: 0.706, y: 0.5},
  // only used in 3-5-2 more in the center
  {id: 15, title: 'Left Midfielder', x: 0.37, y: 0.5},
  {id: 16, title: 'Right Midfielder', x: 0.63, y: 0.5},
  // forwards
  {id: 17, title: 'Attacking Left Midfielder', x: 0.294, y: 0.385},
  {id: 18, title: 'Attacking Right Midfielder', x: 0.706, y: 0.385},
  {id: 19, title: 'Attacking Center Midfielder', x: 0.5, y: 0.385},
  // attack positions --------------------------------------------------------------------------------------------------
  {id: 20, title: 'Left Winger', x: 0.11, y: 0.272},
  {id: 21, title: 'Right Winger', x: 0.89, y: 0.272},
  {id: 22, title: 'Left Inside Winger', x: 0.37, y: 0.175},
  {id: 23, title: 'Right Inside Winger', x: 0.63, y: 0.175},
  {id: 24, title: 'Center Forward', x: 0.5, y: 0.175},
]

export const Formations: Record<string, any> = {
  // 4-chain defense
  '4-2-3-1': [
    // defense
    1, 2, 3, 4,
    // middle
    8, 9, 19,
    // attack
    20, 21, 24
  ],
  '4-3-3': [
    // defense
    1, 2, 3, 4,
    // middle
    10, 13, 14,
    // attack
    20, 21, 24
  ],
  '4-3-1-2': [
    // defense
    1, 2, 3, 4,
    // middle
    10, 13, 14, 19,
    // attack
    22, 23
  ],
  '4-4-2': [
    // defense
    1, 2, 3, 4,
    // middle
    8, 9, 11, 12,
    // attack
    22, 23
  ],
  '4-2-2-2': [
    // defense
    1, 2, 3, 4,
    // middle
    8, 9, 17, 18,
    // attack
    22, 23
  ],
  // 3-chain defense
  '3-3-3-1': [
    // defense
    5, 6, 7,
    // middle
    10, 11, 12, 19,
    // attack
    20, 21, 24
  ],
  '3-4-3': [
    // defense
    5, 6, 7,
    // middle
    8, 9, 11, 12,
    // attack
    20, 21, 24
  ],
  '3-4-1-2': [
    // defense
    5, 6, 7,
    // middle
    8, 9, 11, 12, 19,
    // attack
    22, 23
  ],
  '3-5-2': [
    // defense
    5, 6, 7,
    // middle
    10, 11, 12, 15, 16,
    // attack
    22, 23
  ],
  '3-3-2-2': [
    // defense
    5, 6, 7,
    // middle
    10, 11, 12, 17, 18,
    // attack
    22, 23
  ]
}

export interface FormationScore {
  formation_name: string,
  field_score: number[][],
  chosen_players: { player_id: number, position_id: number }[]
}


