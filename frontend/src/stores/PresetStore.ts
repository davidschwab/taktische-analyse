import {defineStore} from 'pinia';
import api from '../api';
import {attributes, positions, Preset} from 'stores/interfaces';

interface PresetStore {
  attributePresets: Preset[],
  positionPresets: Preset[],
  newAttributePreset: Preset,
  newPositionPreset: Preset,
  edAttributePreset: Preset,
  edPositionPreset: Preset
}

export const usePresetStore = defineStore('PresetStore', {
  state: (): PresetStore => ({
    attributePresets: [],
    positionPresets: [],
    newAttributePreset: {name: '', matrices: []},
    newPositionPreset: {name: '', matrices: []},
    edAttributePreset: {name: '', matrices: []},
    edPositionPreset: {name: '', matrices: []}
  }),
  getters: {
    getAttributePresets: (state): Preset[] => {
      return state.attributePresets
    },
    getAttributePreset: (state): (name: string) => Preset[] => {
      return (name: string) => state.attributePresets.filter((attributePreset) => attributePreset.name === name) as Preset[]
    },
    getNewAttributePreset: (state): Preset => {
      const preset = state.newAttributePreset
      for (const attribute of attributes) {
        preset.matrices.push({name: attribute, array: Array.from(Array(4), () => Array(5).fill(-1))})
      }
      return preset
    },
    getEditAttributePreset: (state): Preset => {
      return state.edAttributePreset
    },
    getPositionPresets: (state): Preset[] => {
      return state.positionPresets
    },
    getPositionPreset: (state): (name: string) => Preset[] => {
      return (name: string) => state.positionPresets.filter((positionPreset) => positionPreset.name === name) as Preset[]
    },
    getNewPositionPreset: (state): Preset => {
      const preset = state.newPositionPreset
      for (const position of positions) {
        preset.matrices.push({name: position, array: Array.from(Array(4), () => Array(5).fill(-1))})
      }
      return preset
    },
    getEditPositionPreset: (state): Preset => {
      return state.edPositionPreset
    },
  },
  actions: {
    requestAttributePresets() {
      api.presets.getAll('attribute').then((resp) => {
        this.attributePresets = resp.data
      }).catch((err) => {
        console.log(err)
      })
    },
    requestAttributePresetByName(name: string, interpolate: boolean) {
      api.presets.get(name, interpolate).then((resp) => {
        const matrices = resp.data.matrices
        for (const preset of matrices) {
          for (let i = 0; i < preset.array.length; i++) {
            for (let j = 0; j < preset.array[i].length; j++) {
              preset.array[i][j] = (preset.array[i][j] == 0) ? -1 : preset.array[i][j]
            }
          }
        }
        this.edAttributePreset = {name: resp.data.name, matrices: matrices}
      }).catch((err) => {
        console.log(err)
      })
    },
    addAttributePreset(preset: Preset) {
      preset.is_room_preset = false
      api.presets.add(preset).then((resp) => {
        this.attributePresets.push(resp.data)
      }).catch((err) => {
        console.log(err)
      })
    },
    editAttributePreset(preset: Preset) {
      api.presets.edit(preset).then((resp) => {
        const index = this.attributePresets.findIndex((preset) => preset.name == resp.data.name)
        if (index !== undefined) {
          this.attributePresets[index] = resp.data
        } else {
          this.attributePresets.push(resp.data)
        }
      }).catch((err) => {
        console.log(err)
      })
    },
    deleteAttributePreset(name: string) {
      api.presets.delete(name).then((resp) => {
        const index = this.attributePresets.findIndex((preset) => preset.name == resp.data.name)
        if (index !== undefined) {
          this.attributePresets.splice(index, 1)
        }
      }).catch((err) => {
        console.log(err)
      })
    },
    requestPositionPresets() {
      api.presets.getAll('position').then((resp) => {
        this.positionPresets = resp.data
      }).catch((err) => {
        console.log(err)
      })
    },
    requestPositionPresetByName(name: string, interpolate: boolean) {
      api.presets.get(name, interpolate).then((resp) => {
        const matrices = resp.data.matrices
        for (const preset of matrices) {
          for (let i = 0; i < preset.array.length; i++) {
            for (let j = 0; j < preset.array[i].length; j++) {
              preset.array[i][j] = (preset.array[i][j] == 0) ? -1 : preset.array[i][j]
            }
          }
        }
        this.edPositionPreset = {name: resp.data.name, matrices: matrices}
      }).catch((err) => {
        console.log(err)
      })
    },
    addPositionPreset(preset: Preset) {
      preset.is_room_preset = true
      api.presets.add(preset).then((resp) => {
        this.positionPresets.push(resp.data)
      }).catch((err) => {
        console.log(err)
      })
    },
    editPositionPreset(preset: Preset) {
      api.presets.edit(preset).then((resp) => {
        const index = this.positionPresets.findIndex((preset) => preset.name == resp.data.name)
        if (index !== undefined) {
          this.positionPresets[index] = resp.data
        } else {
          this.positionPresets.push(resp.data)
        }
      }).catch((err) => {
        console.log(err)
      })
    },
    deletePositionPreset(name: string) {
      api.presets.delete(name).then((resp) => {
        const index = this.positionPresets.findIndex((preset) => preset.name == resp.data.name)
        if (index !== undefined) {
          this.positionPresets.splice(index, 1)
        }
      }).catch((err) => {
        console.log(err)
      })
    },
  }
})

