import {defineStore} from 'pinia';
import api from '../api';
import {Rating} from 'stores/interfaces';

interface RatingStore {
  ratings: Rating[],
  newRating: Rating
}

export const useRatingStore = defineStore('RatingStore', {
  state: (): RatingStore => ({
    ratings: [],
    newRating: {
      id : -1,
      player_id: 0,
      // TODO: event id
      creation_datetime: -1,
      time_active: -1,
      aggression: -1,
      anticipation: -1,
      bravery: -1,
      composure: -1,
      concentration: -1,
      consistency: -1,
      decisions: -1,
      determination: -1,
      creativity: -1,
      flair: -1,
      mental_resistance: -1,
      leadership: -1,
      positioning: -1,
      teamwork: -1,
      vision: -1,
      work_rate: -1,
      acceleration: -1,
      agility: -1,
      balance: -1,
      jumping_reach: -1,
      endurance: -1,
      pace: -1,
      strength: -1,
      corners: -1,
      dribbling: -1,
      finishing: -1,
      first_touch: -1,
      heading: -1,
      shooting_accuracy: -1,
      shooting_power: -1,
      marking: -1,
      short_passing: -1,
      ranged_passing: -1,
      tackling: -1,
      technique: -1,
      free_kicks: -1,
      penalties: -1,
      long_throws: -1
    }
  }),
  getters: {
    getRatings: (state): Rating[] => {
      return state.ratings
    },
    getRating: (state): (playerId: number) => Rating[] => {
      return (playerId: number) => state.ratings.filter((ratings) => ratings.player_id === playerId) as Rating[]
    },
    getNewRating: (state): Rating => {
      return state.newRating
    }
  },
  actions: {
    requestRatingsById(id: number) {
      api.ratings.get(id).then((resp) => {
        this.ratings = resp.data
      }).catch((err) => {
        console.log(err)
      })
    },

    requestRatings() {
      api.ratings.getAll().then((resp) => {
        this.ratings = resp.data
      }).catch((err) => {
        console.log(err)
      })
    },

    addRating(rating: Rating) {
      api.ratings.addRating(rating).then((resp) => {
        this.ratings.push(resp.data)
      }).catch((err) => {
        console.log(err)
      })
    },
    deleteRating(rating_id: number) {
      console.log(rating_id)
      api.ratings.deleteRating(rating_id).then((resp) => {
        const index = this.ratings.findIndex(r => r.id === resp.data.id)
        if (index > -1) {
          this.ratings.splice(index, 1)
        }
      }).catch((err) => {
        console.log(err)
      })
    },

    resetNewRating() {
      this.newRating = {
        player_id: 0,
        // TODO: event id
        creation_datetime: -1,
        time_active: -1,
        aggression: -1,
        anticipation: -1,
        bravery: -1,
        composure: -1,
        concentration: -1,
        consistency: -1,
        decisions: -1,
        determination: -1,
        creativity: -1,
        flair: -1,
        mental_resistance: -1,
        leadership: -1,
        positioning: -1,
        teamwork: -1,
        vision: -1,
        work_rate: -1,
        acceleration: -1,
        agility: -1,
        balance: -1,
        jumping_reach: -1,
        endurance: -1,
        pace: -1,
        strength: -1,
        corners: -1,
        dribbling: -1,
        finishing: -1,
        first_touch: -1,
        heading: -1,
        shooting_accuracy: -1,
        shooting_power: -1,
        marking: -1,
        short_passing: -1,
        ranged_passing: -1,
        tackling: -1,
        technique: -1,
        free_kicks: -1,
        penalties: -1,
        long_throws: -1
      }
    }
  }
})

