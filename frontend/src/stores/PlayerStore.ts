import {defineStore} from 'pinia';
import api from '../api';
import {Player, FormationScore} from 'stores/interfaces';

interface PlayerStore {
  players: Player[],
  newPlayer: Player,
  fieldScore: [],
  positionScores: Record<string, any>,
  bestFormations: FormationScore[]
}

export const usePlayerStore = defineStore('PlayerStore', {
  state: (): PlayerStore => ({
    players: [],
    newPlayer: {
      id: null,
      first_name: '',
      last_name: '',
      sex: '',
      weight: null,
      height: null,
      date_of_birth: '',
      tricot_number: null,
      past_positions: 'front',
    },
    fieldScore: [],
    positionScores: {},
    bestFormations: []
  }),
  getters: {
    getPlayers: (state): Player[] => {
      return state.players
    },
    getPlayer: (state): (playerId: number) => Player => {
      return (playerId: number) => {
        const player = state.players.find((player) => player.id === playerId)
        return (player == undefined) ? (state.newPlayer) : player
      }
    },
    getFieldScore: (state): [] => {
      return state.fieldScore
    },
    getBestFormations: (state): FormationScore[] => {
      return state.bestFormations
    },
    getPositionScores: (state) => {
      return state.positionScores
    }
  },
  actions: {
    requestPlayer(id: number) {
      api.players.get(id).then((resp) => {
        const index = this.players.findIndex((p) => p.id == resp.data.id)
        if (index > -1) {
          this.players[index] = resp.data
        } else {
          this.players.push(resp.data)
        }
      }).catch((err) => {
        console.log(err)
      })
    },

    requestPlayers() {
      api.players.getAll().then((resp) => {
        this.players = resp.data
      }).catch((err) => {
        console.log(err)
      })
    },

    requestPlayerFieldScore(id: number, preset: string) {
      api.players.getFieldScore(id, preset).then((resp) => {
        this.fieldScore = resp.data
      }).catch((err) => {
        console.log(err)
      })
    },

    cleanFieldScore() {
      this.fieldScore = []
    },
    cleanPositionScore() {
      this.positionScores = {}
    },

    requestPlayerPositionScores(id: number, attributePreset: string, positionPreset: string) {
      api.players.getPositionScores(id, attributePreset, positionPreset).then((resp) => {
        this.positionScores = resp.data
      }).catch((err) => {
        console.log(err)
      })
    },

    addPlayer(player: Player) {
      api.players.addPlayer(player).then((resp) => {
        this.players.push(resp.data)
      }).catch((err) => {
        console.log(err)
      })
    },

    editPlayer(player: Player) {
      api.players.edit(player).then((resp) => {
        const index = this.players.findIndex((player) => player.id == resp.data.id)
        if (index !== undefined) {
          this.players[index] = resp.data
          return this.players[index]
        } else {
          this.players.push(resp.data)
        }
      }).catch((err) => {
        console.log(err)
      })
    },

    deletePlayer(id: number) {
      api.players.delete(id).then((resp) => {
        const index = this.players.findIndex((player) => player.id == resp.data.id)
        if (index !== undefined) {
          this.players.splice(index, 1)
        }
      }).catch((err) => {
        console.log(err)
      })
    },

    getBestFormation(playerIds: number[], attributePreset: string, positionPreset: string, positionIds: number[], formation: string, goalKeeperId: number | null | undefined) {
      api.players.getBestFormation(playerIds, attributePreset, positionPreset, positionIds).then((resp) => {
        resp.data.formation_name = formation
        resp.data.chosen_players.push({player_id: goalKeeperId, position_id: 0})
        this.bestFormations.push(resp.data)
      }).catch((err) => {
        console.log(err)
      })
    }
  }
})

