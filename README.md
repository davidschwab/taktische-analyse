# Taktische Analyse



## Wie bringe ich das ganze zum laufen?

#### Frontend Entwicklung

Installiere eine aktuelle version von Nodejs (16 lts) und Python (3.10).

Installiere Quasar cli mittels
```npm i -g @quasar/cli```

Wechsel in den ```frontend``` Ordner

Installiere die Abhänigkeiten ```npm i```

Führe den Frontendentwicklungsserver mit ```quasar dev``` aus

#### Backend Entwicklung


Installiere die Abhänigkeiten ```pip install -r requirements.txt```

Nun kannst du die ```main.py``` ausführen oder
für einen Backendserver mit Code hot-swapping ```uvicorn main:app --reload```