"""
In dieser Klasse definieren wir die Methoden um auf die Datenbank zuzugreifen
und Werte zu speichern/abzurufen.
Zusätzlich können auch Dummy Daten erstellt werden, wenn die entsprechende
Zeile einkommentiert wird.
"""

import pandas as pd
from datetime import datetime

from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker

import model.ORM
from model.DTO import *
from model.ORM import *


class DBInterface:

    def __init__(self, db_path: str = "sqlite:///:memory:"):
        self.engine = create_engine(db_path, future=True)
        model.ORM.Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(self.engine)

        # fill database with dummy data Die untere zeile für Dummy Daten einkommentieren
        # self.fill_database()

    # Players

    def get_players(self):
        query = select(Player)
        with self.Session() as session, session.begin():
            players = [p.to_dict() for p in
                       session.execute(query).scalars().all()]
            return players

    def create_player(self, params: PlayerDTO):
        player = Player(**params)
        with self.Session() as session, session.begin():
            session.add(player)
            session.flush()
            player = player.to_dict()
            return player

    def get_player_by_id(self, player_id: int):
        query = select(Player).filter(Player.id == player_id)
        with self.Session() as session, session.begin():
            player = session.execute(query).scalars().one()
            return player.to_dict()

    def update_player(self, player_id: int, updated_values: PlayerDTO):
        with self.Session() as session, session.begin():
            player = session.query(Player).filter(Player.id == player_id).one()

            for key in updated_values:
                setattr(player, key, updated_values.get(key))

            session.add(player)
            session.flush()
            return player.to_dict()

    def delete_player(self, player_id: int):
        with self.Session() as session, session.begin():
            player = session.query(Player).filter(Player.id == player_id).one()
            session.delete(player)
            session.flush()
            return player.id

    # Ratings

    def create_rating(self, params: RatingDTO):
        rating = Rating(**params)
        # TODO: add creation_datetime to RatingDTO or keep it like this?
        rating.creation_datetime = datetime.now()
        for key, value in rating.to_dict().items():
            if key == 'player_id' or key == 'creation_datetime':
                continue
            # set attributes, that have no measurement to None
            if value == -1:
                setattr(rating, key, None)

        with self.Session() as session, session.begin():
            session.add(rating)
            session.flush()
            rating = rating.to_dict()
            return rating

    def get_ratings(self):
        query = select(Rating)
        with self.Session() as session, session.begin():
            ratings = [rating.to_dict() for rating in
                       session.execute(query).scalars().all()]
            return ratings

    def get_ratings_by_player_id(self, player_id: int):
        query = select(Rating).filter(Rating.player_id == player_id)
        with self.Session() as session, session.begin():
            ratings = [rating.to_dict() for rating in
                       session.execute(query).scalars().all()]
            return ratings

    def get_rating_by_rating_id(self, rating_id: int):
        query = select(Rating).filter(Rating.id == rating_id)
        results = self.get_via_query(query)
        return results[0] if results else None

    def delete_rating_by_id(self, rating_id: int):
        rating = self.get_rating_by_rating_id(rating_id)
        if rating:
            with self.Session(expire_on_commit=False) as session, session.begin():
                session.delete(rating)
                session.flush()
                return rating.id

    # Weight Matrix

    def get_weight_matrix_by_id(self, matrix_id: int) -> WeightMatrix:
        query = select(WeightMatrix).filter(WeightMatrix.id == matrix_id)
        results: list = self.get_via_query(query)
        return results[0] if results else None

    def add_weight_matrix(self, weight_matrix_dto: WeightMatrixDTO):
        weight_matrix = WeightMatrix.from_dto(weight_matrix_dto)
        with self.Session() as session, session.begin():
            session.add(weight_matrix)
            session.flush()
            return weight_matrix.id

    def update_weight_matrix(self, matrix_id: int,
                             weight_matrix_dto: WeightMatrixDTO):
        with self.Session(expire_on_commit=False) as session, session.begin():
            matrix: WeightMatrix = session.query(WeightMatrix).filter(
                WeightMatrix.id == matrix_id).one()

            matrix.name = weight_matrix_dto["name"]
            matrix.numpy_array = np.array(weight_matrix_dto["array"])

            session.add(matrix)
            session.flush()

            return matrix.to_dict()

    def delete_weight_matrix(self, matrix_id: int):
        with self.Session(expire_on_commit=False) as session, session.begin():
            matrix = session.query(WeightMatrix).filter(
                WeightMatrix.id == matrix_id).one()
            session.delete(matrix)
            session.flush()
            return matrix.id

    # Preset

    def get_all_preset_dicts(self):
        with self.Session() as session, session.begin():
            presets = session.execute(select(Preset)).scalars().all()
            return [p.to_dict(interpolate=True) for p in presets]

    def get_preset_dict_by_name(self, preset_name: str, interpolate=False):
        query = select(Preset).filter(Preset.name == preset_name)
        with self.Session(expire_on_commit=False) as session, session.begin():
            result: list = session.execute(query).scalars().all()
            return result[0].to_dict(interpolate) if result else None

    def add_preset(self, preset_name: str, is_room_preset: bool,
                   weight_matrix_ids: list[int]):
        with self.Session(expire_on_commit=False) as session, session.begin():
            matrix_query = select(WeightMatrix).filter(
                WeightMatrix.id.in_(weight_matrix_ids))
            matrices = session.execute(matrix_query).scalars().all()
            preset = Preset(name=preset_name, is_room_preset=is_room_preset,
                            matrices=matrices)
            preset.matrices.extend(matrices)
            session.add(preset)
            session.flush()
            return preset.to_dict(interpolate=True)

    def delete_preset(self, preset_name: str):
        with self.Session(expire_on_commit=False) as session, session.begin():
            preset = session.query(Preset).filter(
                Preset.name == preset_name).one()
            session.delete(preset)
            session.flush()
            return preset_name

    # https://docs.sqlalchemy.org/en/14/orm/session_basics.html#querying-2-0-style
    def get_via_query(self, query):
        with self.Session(expire_on_commit=False) as session, session.begin():
            return session.execute(query).scalars().all()

    def fill_database(self):
        load_excel = False
        # fill database with lucas' data
        if load_excel:
            data = pd.read_excel('./model/lucas_ratings.xlsx')

            for index, row in enumerate(data.iterrows()):
                player = self.create_player(
                    PlayerDTO(
                        first_name=row[1]['first_name'],
                        last_name=row[1]['last_name'],
                        sex="male",
                        weight=np.random.randint(70_000, 100_000),
                        height=np.random.randint(160, 200),
                        date_of_birth=date.fromisocalendar(
                            year=np.random.randint(1980, 2000),
                            week=np.random.randint(1, 52),
                            day=np.random.randint(1, 7)
                        ),
                        tricot_number=row[1]['tricot_number'],
                        past_positions="-"
                    )
                )

                player_id = player['id']

                rating = RatingDTO(player_id=player_id)
                for key in RatingDTO.__annotations__.keys():
                    if key == "player_id":
                        continue
                    if key in data.keys():
                        if data[key][index] is not np.NaN:
                            rating[key] = data[key][index]

                self.create_rating(rating)

        if not self.get_preset_dict_by_name('Default_Ratings'):
            from model.Dummy import rating_matrices, position_matrices
            ids = [self.add_weight_matrix(matrix) for matrix in
                   rating_matrices]
            self.add_preset("Default_Ratings", is_room_preset=False,
                            weight_matrix_ids=ids)

        if not self.get_preset_dict_by_name('Default_Positions'):
            ids = [self.add_weight_matrix(matrix) for matrix in
                   position_matrices]
            self.add_preset("Default_Positions", is_room_preset=True,
                            weight_matrix_ids=ids)

            print("initialized database with dummy data.")
