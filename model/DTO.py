"""
Diese Klasse definiert die Objekte,
die in der RESTapi mit vom Frontend empfangen werden.
Diese werden meist genutzt,
um sie direkt zu Datenbank Einträgen zu verarbeiten.
"""
from datetime import date
from typing import TypedDict


class PlayerDTO(TypedDict):
    first_name: str
    last_name: str
    sex: str
    weight: int
    height: int
    date_of_birth: date  # https://fastapi.tiangolo.com/tutorial/extra-data-types/
    tricot_number: int
    past_positions: str | None


class RatingDTO(TypedDict):
    player_id: int
    time_active: int | None
    aggression: int | None
    anticipation: int | None
    bravery: int | None
    composure: int | None
    concentration: int | None
    consistency: int | None
    decisions: int | None
    determination: int | None
    creativity: int | None
    flair: int | None
    mental_resistance: int | None
    leadership: int | None
    positioning: int | None
    teamwork: int | None
    vision: int | None
    work_rate: int | None
    acceleration: int | None
    agility: int | None
    balance: int | None
    jumping_reach: int | None
    endurance: int | None
    pace: int | None
    strength: int | None
    corners: int | None
    dribbling: int | None
    finishing: int | None
    first_touch: int | None
    heading: int | None
    shooting_accuracy: int | None
    shooting_power: int | None
    marking: int | None
    short_passing: int | None
    ranged_passing: int | None
    tackling: int | None
    technique: int | None
    free_kicks: int | None
    penalties: int | None
    long_throws: int | None


class FormationConfigurationDTO(TypedDict):
    player_ids: list[int]
    position_ids: list[int]
    attribute_preset: str
    position_preset: str


class WeightMatrixDTO(TypedDict):
    name: str
    array: list[list[float]]


class PresetDTO(TypedDict):
    name: str
    is_room_preset: bool
    matrices: list[WeightMatrixDTO]
