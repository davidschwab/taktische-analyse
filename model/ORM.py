"""
Diese Datei definiert das Datenbank schema.
Dies ist Notwendig um konsistent mit der Datenbank interagieren zu können.
"""
import numpy as np

from computations.weight_matrices import interpolate_from_ones

from sqlalchemy import Column, String, Integer, ForeignKey, Date, DateTime, Boolean
from sqlalchemy.orm import relationship

from sqlalchemy.orm import declarative_base

from model.DTO import WeightMatrixDTO

Base = declarative_base()


class Rating(Base):
    __tablename__ = 'rating'

    id = Column(Integer, primary_key=True)
    player_id = Column(Integer, ForeignKey('player.id'))
    player = relationship("Player", back_populates="ratings")
    creation_datetime = Column(DateTime, nullable=False)
    time_active = Column(Integer)  # in seconds
    aggression = Column(Integer)
    anticipation = Column(Integer)
    bravery = Column(Integer)
    composure = Column(Integer)
    concentration = Column(Integer)
    consistency = Column(Integer)
    decisions = Column(Integer)
    determination = Column(Integer)
    creativity = Column(Integer)
    flair = Column(Integer)
    mental_resistance = Column(Integer)
    leadership = Column(Integer)
    positioning = Column(Integer)
    teamwork = Column(Integer)
    vision = Column(Integer)
    work_rate = Column(Integer)
    acceleration = Column(Integer)
    agility = Column(Integer)
    balance = Column(Integer)
    jumping_reach = Column(Integer)
    endurance = Column(Integer)
    pace = Column(Integer)
    strength = Column(Integer)
    corners = Column(Integer)
    dribbling = Column(Integer)
    finishing = Column(Integer)
    first_touch = Column(Integer)
    heading = Column(Integer)
    shooting_accuracy = Column(Integer)
    shooting_power = Column(Integer)
    marking = Column(Integer)
    short_passing = Column(Integer)
    ranged_passing = Column(Integer)
    tackling = Column(Integer)
    technique = Column(Integer)
    free_kicks = Column(Integer)
    penalties = Column(Integer)
    long_throws = Column(Integer)

    def to_dict(self):
        return {
            "id": self.id,
            "player_id": self.player_id,
            "creation_datetime": self.creation_datetime,
            "time_active": self.time_active,
            "aggression": self.aggression,
            "anticipation": self.anticipation,
            "bravery": self.bravery,
            "composure": self.composure,
            "concentration": self.concentration,
            "consistency": self.consistency,
            "decisions": self.decisions,
            "determination": self.determination,
            "creativity": self.creativity,
            "flair": self.flair,
            "mental_resistance": self.mental_resistance,
            "leadership": self.leadership,
            "positioning": self.positioning,
            "teamwork": self.teamwork,
            "vision": self.vision,
            "work_rate": self.work_rate,
            "acceleration": self.acceleration,
            "agility": self.agility,
            "balance": self.balance,
            "jumping_reach": self.jumping_reach,
            "endurance": self.endurance,
            "pace": self.pace,
            "strength": self.strength,
            "corners": self.corners,
            "dribbling": self.dribbling,
            "finishing": self.finishing,
            "first_touch": self.first_touch,
            "heading": self.heading,
            "shooting_accuracy": self.shooting_accuracy,
            "shooting_power": self.shooting_power,
            "marking": self.marking,
            "short_passing": self.short_passing,
            "ranged_passing": self.ranged_passing,
            "tackling": self.tackling,
            "technique": self.technique,
            "free_kicks": self.free_kicks,
            "penalties": self.penalties,
            "long_throws": self.long_throws
        }


class Player(Base):
    __tablename__ = 'player'

    id = Column(Integer, primary_key=True)
    ratings = relationship("Rating", back_populates="player")
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)

    sex = Column(String, nullable=False)
    weight = Column(Integer, nullable=False)  # in grams
    height = Column(Integer, nullable=False)  # in cm
    date_of_birth = Column(Date, nullable=False)
    tricot_number = Column(Integer, nullable=False)
    past_positions = Column(String)  # this might need to be changed later

    def __repr__(self):
        return f"{self.id}: {self.first_name} {self.last_name}"

    def to_dict(self):
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "sex": self.sex,
            "weight": self.weight,
            "height": self.height,
            "date_of_birth": self.date_of_birth,
            "tricot_number": self.tricot_number,
            "past_positions": self.past_positions
        }


class WeightMatrix(Base):
    __tablename__ = 'weight_matrix'

    id = Column(Integer, primary_key=True)
    preset_id = Column(Integer, ForeignKey('preset.name'))
    preset = relationship("Preset", back_populates="matrices")

    name = Column(String, nullable=False)
    array = Column(String, nullable=False)

    matrix_shape_x = Column(Integer, nullable=False)
    matrix_shape_y = Column(Integer, nullable=False)

    @classmethod
    def from_dto(cls, dto: WeightMatrixDTO):
        np_array = np.array(dto["array"])
        # -1 values are turned into 0
        np_array[np_array == -1] = 0
        shape_x, shape_y = np_array.shape
        return cls(name=dto["name"],
                   array=np_array.tostring(),
                   matrix_shape_x=shape_x,
                   matrix_shape_y=shape_y)

    @property
    def matrix_shape(self):
        return self.matrix_shape_x, self.matrix_shape_y

    @property
    def numpy_array(self) -> np.ndarray:
        return np.frombuffer(self.array).reshape(self.matrix_shape)

    @numpy_array.setter
    def numpy_array(self, new_array: np.ndarray):
        self.array = new_array.tostring()
        self.matrix_shape_x, self.matrix_shape_y = new_array.shape

    def to_dict(self, interpolate=False):
        array = self.numpy_array
        array = interpolate_from_ones(array).tolist() if interpolate else array.tolist()

        return {"id": self.id, "name": self.name, "array": array,
                "shape": self.matrix_shape}


class Preset(Base):
    __tablename__ = 'preset'

    name = Column(String, primary_key=True)
    is_room_preset = Column(Boolean)
    matrices: list[WeightMatrix] = relationship("WeightMatrix", back_populates="preset",
                                                cascade="all, delete",
                                                passive_deletes=True)

    def to_dict(self, interpolate=False):
        matrices = [m.to_dict(interpolate=interpolate) for m in self.matrices]
        return {"name": self.name, "is_room_preset": self.is_room_preset, "matrices": matrices}
